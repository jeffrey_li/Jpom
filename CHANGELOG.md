# 版本日志

## 2.4.1

### 新增功能

1. 【Agent】新增线程列表监控(感谢@其锋)
2. 【Agent】新增节点脚本模板(感谢@其锋)
3. 【Server】新增所有页面添加公共Html代码
4. 新增Tomcat管理
5. 【Agent】导入证书文件新增对cer、crt文件支持
6. 【Agent】导入项目包时指出多压缩包[tar|bz2|gz|zip|tar.bz2|tar.gz] (感谢@群友)
7. 【Agent】新增配置控制台日志文件编码格式（详情查看extConfig.yml）

### 解决BUG、优化功能
    
1. 【Server】节点首页，右上角管理路径错误(感谢@其锋)
2. 【Server】查看用户操作日志支持筛选用户
3. 【Server】页面数据路径权限判断修护(感谢@Will)
4. 【Agent】优化获取进程监听端口的，防止卡死
5. 文件的读写锁不使用 synchronized关键字提高效率
6. 优化数据id字段的输入限制，数字+字母+中划线+下划线（感谢@JAVA jesion）
7. 【Agent】连接JVM失败则跳过（感谢@JAVA jesion）
8. 【Server】编辑用户页面优化选择授权项目
9. 【Agent】项目Jvm参数和Args参数兼容回车符（感谢@牛旺）

-----------------------------------------------------------

## 2.4.0

### 新增功能

1. 首页进程列表显示属于Jpom项目名称(感谢@〓下页)
2. 多节点统一管理（插件模式）
3. 证书解析支持cer 证书(感谢@JAVA jesion)
4. 新增记录用户操作日志[采用H2数据库]（感谢@〓下页）
5. 节点分发功能、合并管理项目(感谢@其锋)

### 解决BUG、优化功能

1. 解析端口信息兼容`:::8084`(感谢@Agoni 、)
2. 进程id解析端口、解析项目名称带缓存
3. 项目分组变更，项目列表及时刷新(感谢@〓下页)
4. 批量上传文件数量进度显示(感谢@群友)
5. linux udp端口信息解析失败(感谢@Ruby)
6. jar模式读取主jar包错误(感谢@其锋)

-----------------------------------------------------------

## 2.3.2

### 新增功能

1. 控制台日志支持配置保留天数
2. 项目列表状态跟随控制台刷新
3. 项目配置页面优化交互流程
4. 项目列表显示正在运行的项目的端口号(感谢@洋芋)
5. 新版的Windows管理命令(感谢@洋芋)
6. 支持类似于Nginx二级代理配置(感谢@№譜樋)
7. 记录启动、重启、停止项目的操作人
8. Jpom 数据路径默认为程序运行的路径(感谢@〓下页)
9. 首页进程监听表格显示端口号(感谢@洋芋)
10. 保存时检查Oss信息是否正确
11. Jpom管理命令新增判断`JAVA_HOME`环境变量
12. 修改用户信息，在线用户需要重新登录

### 解决BUG、优化功能

1. 修改WebHooks 不生效
2. 初始化系统白名单初始化失败(感谢@洋芋)
3. 指定Cookie名称防止名称相同被踢下线(感谢@洋芋)
4. 优化未加载到tools.jar的提示(感谢@№譜樋)
5. 构建按钮移动到文件管理页面中
6. 优化nginx列表显示数据、取消nginx快捷配置
7. 证书管理页面交互优化
8. 取消安全模式功能（有更完善的权限代替）
9. 管理员不能修改自己的信息

-----------------------------------------------------------

## 2.3.1

#### 新增功能

1. 添加创建项目判断项目id是否被占用
2. 项目列表中添加悬停突出显示效果
3. 生产环境中检查Jpom 运行标识和项目id是否冲突
4. windows 管理命令支持停止Jpom
5. 防止暴力登录新增限制ip登录失败次数
6. 用户前台输入密码传输加密（感谢@JAVA jesion）
7. 首页页面自动刷新按钮状态记忆功能（感谢@Mark）
8. Jpom启动成功会自动在数据目录中创建进程id信息文件如`pid.27936`
9. 证书管理支持导出、查看代码模板功能

#### 解决BUG 

1. 解决配置JVM、ARGS时，不能获取到程序运行信息bug(感谢@Agoni 、)
2. 减少登录图形验证码干扰线(感谢@Mark)
3. 项目编辑页面JVM、ARGS调整为多行文本(感谢@JAVA jesion)
4. jar模式MainClass非必填
4. 优化JDK32位和64位冲突时自动跳过(感谢@13145597)
5. 用户授权项目权限不足问题

#### 升级注意事项
1. 由2.2及以下升级到 2.3.x 需要手动删除Jpom数据目录中的`data/user.json` 文件、所有用户账户信息将失效需要重新添加

-----------------------------------------------------------

## 2.2 

1. 解决批量上传文件造成卡死的问题
2. 控制台读取自动识别文件编码格式
3. 退出登录出现异常页面
4. 根据对应权限显示对应菜单
5. 系统管理员可以在线解锁锁定的用户

-----------------------------------------------------------

## 2.1 

1. 全面取消调用命令文件执行
2. 静态资源缓存问题
3. 首页监控图表更新
4. 多处细节优化
5. 分别支持ClassPath和Jar模式
6. 证书文件支持验证私钥是否匹配

-----------------------------------------------------------

## 2.0 

1. 优化安全问题
2. 兼容windows
3. 使用JVM获取运行状态